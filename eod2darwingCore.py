#!/bin/env python 
import argparse
import csv
import gzip 
import io
import collections

eodFields = [
    'BASISOFRECORD',
    'INSTITUTIONCODE',
    'COLLECTIONCODE',
    'CATALOGNUMBER',
    'OCCURRENCEID',
    'RECORDEDBY',
    'YEAR',
    'MONTH',
    'DAY',
    'PUBLISHINGCOUNTRY',
    'COUNTRY',
    'STATEPROVINCE',
    'COUNTY',
    'DECIMALLATITUDE',
    'DECIMALLONGITUDE',
    'LOCALITY',
    'KINGDOM',
    'PHYLUM',
    'CLASS',
    'ORDER',
    'FAMILY',
    'GENUS',
    'SPECIFICEPITHET',
    'SCIENTIFICNAME',
    'VERNACULARNAME',
    'TAXONREMARKS',
    'INDIVIDUALCOUNT'
]

darwingOcurrence = ['id','institutionCode','collectionCode','basisOfRecord','occurrenceID'
    ,'catalogNumber','recordedBy','individualCount','year','month','day','country','stateProvince'
    ,'county','locality','decimalLatitude','decimalLongitude','scientificName','kingdom','phylum'
    ,'class','order','family','genus','specificEpithet','publishingCountry','publishingCountry',
    'taxonRemarks']

darwingOcurrenceMap = collections.OrderedDict( {
    'id': 'CATALOGNUMBER',																								
    'institutionCode': 'INSTITUTIONCODE',
    'collectionCode': 'COLLECTIONCODE',
    'basisOfRecord': 'BASISOFRECORD',
    'occurrenceID': 'OCCURRENCEID',
    'catalogNumber': 'CATALOGNUMBER',
    'recordedBy': 'RECORDEDBY',
    'individualCount': 'INDIVIDUALCOUNT',
    'year': 'YEAR',
    'month': 'MONTH',
    'day': 'DAY',
    'country': 'COUNTRY',
    'stateProvince': 'STATEPROVINCE',
    'county': 'COUNTY',
    'locality': 'LOCALITY',
    'decimalLatitude': 'DECIMALLATITUDE',
    'decimalLongitude': 'DECIMALLONGITUDE',
    'scientificName': 'SCIENTIFICNAME',
    'kingdom': 'KINGDOM',
    'phylum': 'PHYLUM',
    'class': 'CLASS',
    'order': 'ORDER',
    'family': 'FAMILY',
    'genus': 'GENUS',
    'specificEpithet': 'SPECIFICEPITHET',
    'publishingCountry': 'PUBLISHINGCOUNTRY',
    'taxonRemarks': 'TAXONREMARKS'
})
records = 0

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Convert EOD file to Darwing Core')
  parser.add_argument('eodFile', help='Path to the compressed (gzip) eod file.')
  parser.add_argument('-v', '--version', help='Version to be generated')
  args = parser.parse_args()

  csv.field_size_limit(500 * 1024 * 1024)
  with gzip.GzipFile(args.eodFile) as uncompressFile:
    inputFile = io.TextIOWrapper(uncompressFile)
    reader = csv.DictReader(inputFile, delimiter=',', quoting=csv.QUOTE_MINIMAL, fieldnames=eodFields)
    with open('occurrence.txt', 'w') as ocurrenceFile:
        ocurrenceWriter = csv.DictWriter(ocurrenceFile, fieldnames=darwingOcurrence,
                                    delimiter='\t', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        ocurrenceWriter.writeheader()
        for eodRow in reader:
            records += 1
            if records == 1 :
                continue # skip header
            darwingRow = {}
            for darwing_field, eod_field in darwingOcurrenceMap.items():
                darwingRow[darwing_field] = eodRow[eod_field]
            ocurrenceWriter.writerow(darwingRow)
