=============================
EOD to Darwing Core Converter
=============================

This utility convert the EOD text file in csv compressed gzip format to the Darwing Core format. 
Darwing Core is a zip file with 3 files:

- ``occurrence.txt`` a tab separated values text file with the ocurrence information
- ``meta.xml`` describes the fields in the ocurrence.txt 
- ``eml.eml`` the metadata file in eml format for ocurrence.txt


Requirements
------------

- Python 2.7
- ``zip`` command


Installation
------------

Download the source code using: 

``git clone ssh://git@bitbucket.org:francisco235/darwingcore-converter.git``


Use
---

In order to generate the ocurrence.txt file from the eod type:

``eod2darwingCore.py eodFile``

where eodFile is, the **csv compressed gzip**, eod file.  
After generation review the eml.eml for changes in contact information and date of publication. 
The darwing core file is generated with the command:

``zip dwca-1.0.zip meta.xml eml.xml occurrence.txt``

Then copy the zip file to the webser 

``scp dwca-1.0.zip motmot02.ornith.cornell.edu:/web/www/ebirddata.ornith.cornell.edu/downloads/gbiff``

Notify Tim Robertson <trobertson@gbif.org> at GBIF that there is a new pusblished version. 
GBIF Team will download this file for harvesting of the infomation. 
